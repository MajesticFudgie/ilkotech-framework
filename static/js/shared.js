$(document).ready(function(){
	$('body').on("click",".areyousure",function(e){
		ays = "Are you sure?";
		if ($(this).attr("data-ays") != "") {
			ays = $(this).attr("data-ays");
		}
		if (!confirm(ays)) {
			e.preventDefault();
		}
	});
    $("textarea.bbcode").sceditor({
        plugins: "bbcode",
		style: "//static.ilkotech.co.uk/css/square.min.css",
		emoticonsRoot:"//static.ilkotech.co.uk/img/"
    });
	setInterval(function(){
		console.log("Performing Login Check");
		$.get("/ajax/login_check",function(data){
			if (data != "") {
				console.log("Invalid Session, redirecting");
				location.href = data;
			}
		});
	},10000);
	
	function ays(prompt) {
		if (!prompt) prokmpt = "Are you sure?";
		return confirm(prompt);
	}
});