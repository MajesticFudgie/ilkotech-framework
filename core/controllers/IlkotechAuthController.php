<?php
class Ilkotech {
	public static function requestAuth($application=false){
		// http://accounts.ilkotech.co.uk/api/auth/request_token
		$url = "https://accounts.ilkotech.co.uk/api/auth/request_token";
		$data = [];
		$data['app_secret'] = Core::$config['site']['appSecret'];
		
		$response = Utils::curlPOST($url,$data);
		$response = json_decode($response,true);
		
		if ($response['result']) {
			$data = [];
			$data['auth_secret'] = $response['auth_secret'];
			$data['auth_public'] = $response['auth_public'];
			$data['auth_url'] = $response['auth_url'];
			$data['application'] = $application;
			if ($application) {
				$data['app_code'] = Utils::genID();
			}
			$data['stamp'] = time();
			$authid = Core::$sql->insert("auth",$data);
			return $authid;
		} else {
			return null;
		}
	}
	public static function getAuth($aid) {
		Core::$sql->where("id",$aid);
		$auth = Core::$sql->get("auth");
		if (count($auth) <= 0) return null;
		return $auth[0];
	}
	public static function findAuth($ac) {
		Core::$sql->where("app_code",$ac);
		$auth = Core::$sql->get("auth");
		if (count($auth) <= 0) return null;
		return $auth[0];
	}
	public static function updateUser($user_hash) {
		// http://accounts.ilkotech.co.uk/api/user/read
		// auth_secret
		
		$url = "https://accounts.ilkotech.co.uk/api/user/read";
		$data['app_secret'] = Core::$config['site']['appSecret'];
		$data['userid'] = $user_hash;
		
		$res = Utils::curlPOST($url,$data);
		$res = json_decode($res,true);
		
		if (!$res['result']) {
			return false;
		}
		
		Core::$sql->where("userid",$user_hash);
		$users = Core::$sql->get("users");
		
		if (count($users) <= 0) {
			// Create them
			Core::$sql->where("username",$res['username']);
			$users = Core::$sql->get("users");
			if (count($users) > 0) return false;
			
			$d = [];
			$d['userid'] = $res['userid'];
			$d['username'] = $res['username'];
			$d['name'] = $res['name'];
			$uid = Core::$sql->insert("users",$d);
		} else {
			// Update an existing user.
			Core::$sql->where("userid",$user_hash);
			$data = [];
			$data['name'] = $res['name'];
			Core::$sql->update("users",$data);
		}
		return true;
	}
	
}
?>
