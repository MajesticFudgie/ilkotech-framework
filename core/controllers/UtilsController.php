<?php
class Utils {
	public static function curlPOST($url,$data = []) {
		$fields = [];
		
		foreach ($data as $k => $v) {
			$fields[$k] = urlencode($v);
		}
		//url-ify the data for the POST
		$fstr="";
		foreach($fields as $key=>$value) { $fstr .= $key.'='.$value.'&'; }
		rtrim($fstr, '&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fstr);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
		
		return $result;
	}
	
	public static function getSetting($var) {
		Core::$sql->where("var",$var);
		$dat = Core::$sql->get("settings");
		if (count($dat) <= 0) return null;
		return $dat[0];
	}
	
	public static function bbcode($text) {
		$cleaned = trim(htmlentities($text));
		Core::$bbcode->parse($cleaned);
		return Core::$bbcode->getAsHtml();
		
	}
	
	public static function genID($length = "10") {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		$charlist = str_split($chars);
		$salt = "";
		for ($i=0; $i < $length; $i++) {
			$salt = $salt.$charlist[array_rand($charlist)];
		}
		return $salt;
	}
	public static function genNumeric($length = "10") {
		$chars = "1234567890";
		$charlist = str_split($chars);
		$salt = "";
		for ($i=0; $i < $length; $i++) {
			$salt = $salt.$charlist[array_rand($charlist)];
		}
		return $salt;
	}
	
	// Allow templates to register CSS and JS
	public static $javascript = [];
	public static $css = [];
	
	public static function addCSS($url,$content=null) {
		self::$css[] = ["url"=>$url,"content"=>$content];
	}
	public static function addJS($url,$content=null) {
		self::$javascript[] = ["url"=>$url,"content"=>$content];
	}
	
	public static function getCSS() {
		return self::$css;
	}
	public static function getJS() {
		return self::$javascript;
	}
	
	public static function textTrim($str,$length=15) {
		$suffix = "…";
		if (strlen($str) > $length-1) return substr($str,0,$length-1).$suffix;
		return $str;
	}
}
?>