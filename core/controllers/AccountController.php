<?php
Class Account {
	public static function getUserByID($id) {
		Core::$sql->where("id",$id);
		$data = Core::$sql->get("users");
		if (count($data) <= 0) return null;
		return $data[0];
	}
	public static function getUserByHash($hash) {
		Core::$sql->where("userid",$hash);
		$data = Core::$sql->get("users");
		if (count($data) <= 0) return null;
		return $data[0];
	}
	public static function getUser($username) {
		Core::$sql->where("username",$username);
		$data = Core::$sql->get("users");
		if (count($data) > 0) {
			return $data[0];
		} else {
			return false;
		}
	}
	public static function getUsers() {
		return Core::$sql->get("users");
	}
	public static function genSalt($length = "15") {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!?#@";
		$charlist = str_split($chars);
		$salt = "";
		for ($i=0; $i < $length; $i++) {
			$salt = $salt.$charlist[array_rand($charlist)];
		}
		return $salt;
	}
	public static function updateDetails($uid,$data) {
		$user = self::getUserByID($uid);
		if ($user==null) return false;
		Core::$sql->where("id",$uid);
		Core::$sql->update("users",$data);
		return true;
	}
}
?>