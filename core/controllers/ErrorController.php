<?php
class Errors {
	private static $errors = [];

	static function err($str){
		self::$errors = array_merge(self::$errors, (array) $str);
	}
	static function hasErrors() {
		if (count(self::$errors) > 0) return true;
		return false;
	}
	static function run(){
		Flight::view()->assign('error',self::$errors);
	}
	
	// Named Errors
	private static $namedError = [];
	static function setErr($id,$val) {
		self::$namedError[$id] = $val;
	}
	static function getErr($id=null) {
		if ($id != null) {
			if (isset(self::$namedError[$id])) {
				return self::$namedError[$id];
			}
			return null;
		} else {
			return self::$namedError;
		}
	}
}
?>