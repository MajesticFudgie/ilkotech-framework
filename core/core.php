<?php
// Ensure we're using SSL
$ssl = false;
if(isset($_SERVER['HTTPS'])) {
    if ($_SERVER['HTTPS'] == "on") {
        $ssl = true;
    }
}
if (!$ssl) {
	//header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	//die();
}
session_start();

// Load Libraries.
include("lib/MysqliDb.php");
include("lib/phpmailer/PHPMailerAutoload.php");
include("lib/flight/flight/Flight.php");
include("lib/smarty/libs/Smarty.class.php");
include("lib/JBBCode/Parser.php");

//include("lib/PhpUserAgent/Source/UserAgentParser.php");
//include("lib/GoogleAuthenticator/lib/GoogleAuthenticator.php");

// Load Controllers in.
foreach(scandir("core/controllers/") as $c) {
	if ($c[0] != ".") include("controllers/".$c);
}

// Load the core
class Core {
	public static $sql = null;
	public static $config = null;
	public static $account = null;
	
	static function start() {
		self::loadConfigs();
		self::setupMySQL();
		self::setupSessions();
	}
	static function loadConfigs(){
		// Vars
		$_CONFIG = [];
		// Load configurations.
		include("config/site.php");
		include("config/mysql.php");
		self::$config = $_CONFIG;
	}
	static function setupMySQL(){
		if (!self::$config['mysql']['enabled']) return;
		$_SQL = null;
		$_SQL = new MysqliDB(self::$config['mysql']);
		$_SQL->setPrefix(self::$config['mysql']['prefix']);
		$_SQL->connect();
		self::$sql = $_SQL;
	}
	static function setupSessions(){
		// Handle sessions.
		self::$account = ["loggedin"=>false];
		if (isset($_SESSION['user'])) {
			if (Account::getUserbyID($_SESSION['user']) != null) {
				$user = Account::getUserByID($_SESSION['user']);
				self::$account = ["loggedin"=>true,"user"=>$user];
			} else {
				unset($_SESSION['user']);
			}
		}
	}
    static function loggedIn() {
		if (isset($_SESSION['user'])) {
			if (Account::getUserbyID($_SESSION['user']) != null) {
				return true;
			}
		}
		return false;
    }
	static function setupCaptcha(){
		if (self::$config['site']['captchaSecret'] != null) {
			Captcha::setSecret(self::$config['site']['captchaSecret']);
			Flight::view()->assign('captcha',Core::$config['site']['captchaKey']);
		}
	}
	static function frontend() {
		// Session stuff
		Flight::view()->assign('account',Core::$account);

		if(Core::$config['site']['maintenance']) {
			Flight::route('*', function(){
				Flight::view()->display('base/offline.tpl');
			});
		} else {
			// Load Routes in
			foreach(scandir("core/routes/") as $c) {
				if ($c[0] != ".") include("routes/".$c);
			}
		}
		// Let the games begin.
		Flight::start();
	}
}

// Add the Smarty Support
Flight::register('view', 'Smarty', array(), function($smarty){
    $smarty->template_dir = 'templates/tpl/';
    $smarty->compile_dir = 'templates/tpl_compiled/';
    $smarty->config_dir = 'core/config/';
    $smarty->cache_dir = 'cache/';
	$smarty->force_compile = true;
	$smarty->configLoad('smarty.conf');
});
Flight::map('render', function($template){
	Errors::run();
    Flight::view()->display($template);
});

Core::start();
?>