<?php
Flight::route('/login/',function(){
	$res = Ilkotech::requestAuth();
	if ($res != null) {
		$auth = Ilkotech::getAuth($res);
		if ($auth != null) {
			Flight::redirect($auth['auth_url']);
		} else {
			die("Error requesting authentication token from Ilkotech! Err #1");
		}
	} else {
		die("Error requesting authentication token from Ilkotech! Err #2");
	}
});
Flight::route('/login/@token',function($token){
	Core::$sql->where("auth_public",$token);
	$auth = Core::$sql->get("auth");
	if (count($auth) <= 0) {
		die("Invalid token.");
	}
	if ($auth[0]['used'] == 1) {
		die("Expired Token");
	}
	$res = Ilkotech::updateUser($auth[0]['user_hash']);
	if ($res) {
		// It all went great!
		Core::$sql->where("userid",$auth[0]['user_hash']);
		$users = Core::$sql->get("users");
		if (count($users) <= 0) {
			die('Unable to find your user :(!');
		}
		
		Core::$sql->where("id",$auth[0]['id']);
		Core::$sql->update("auth",["used"=>true]);
		
		$_SESSION['user'] = $users[0]['id'];
		if ($auth[0]['application']) {
			$sess = Account::createSession($_SESSION['user'],$_SERVER['REMOTE_ADDR']);
			Core::$sql->where("id",$auth[0]['id']);
			Core::$sql->update("auth",["user"=>$_SESSION['user'],"session"=>$sess['id']]);
			Flight::render('close.tpl');
		} else {
			Flight::redirect('/');
		}
	} else {
		// All went wrong!
		die("Unable to authenticate, possible username conflict.");
	}
});
Flight::route('POST /api/ilkotech/login',function(){
	// we recieve the auth id and user hash to do as we please with
	$auth_secret = $_POST['auth_secret'];
	$user_hash = $_POST['user_hash'];
	
	// Update our auth
	Core::$sql->where("auth_secret",$auth_secret);
	$auth = Core::$sql->get("auth");
	
	if (count($auth) <= 0) {
		$payload = [];
		$payload['result'] = false;
		$payload['message'] = "Invalid Auth Key";
		die(json_encode($payload));
	}
	
	Core::$sql->where("id",$auth[0]['id']);
	Core::$sql->update("auth",["user_hash"=>$user_hash]);
	
	$login = "http://games.thomas-edwards.me/login/".$auth[0]['auth_public'];
	
	$payload = [];
	$payload['result'] = true;
	$payload['login_url'] = $login;
	
	die(json_encode($payload));
});
?>