<?php
// If logged in redirect to account center otherwise redirect to website home.
Flight::route('/(home)', function(){
	if (!core::loggedIn()) {
		die();
		return;
	}
	Flight::render('home.tpl');
});

// Checks if the person is still logged in.
// Poll this URL to check if the users logged in.
// Returns nothing when logged in, returns new location when logged out.
Flight::route('/ajax/login_check',function(){
	if (!Core::loggedIn()) die("/login");
});

Flight::route('/logout',function(){
	if (isset($_SESSION['user'])) unset($_SESSION['user']);
	Flight::view()->assign("message","Logged out!");
	Flight::render('notice.tpl');
});


// Defaults
Flight::map('notFound', function(){
	Flight::render('base/404.tpl');
});
if (!Core::$config['site']['production']) {
	Flight::map('error', function(Exception $ex){
		// Handle error
		$error = [];
		$error['message'] = $ex->getMessage();
		Flight::view()->assign("error",$error);
		Flight::render('base/error.tpl');
	});
}
?>
