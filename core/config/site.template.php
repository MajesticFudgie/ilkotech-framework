<?php
// Add your own stuff here but dont remove the initial var and maintenance var!
$_CONFIG['site'] = [];
$_CONFIG['site']['maintenance'] = false;
$_CONFIG['site']['production'] = false;
$_CONFIG['site']['static'] = "static.ilkotech.co.uk";

// Google Captcha Secret - null to disable.
$_CONFIG['site']['captchaSecret'] = "";
// Google Captcha Site Key - Ignore if the secret is 'null'
$_CONFIG['site']['captchaKey'] = "";

// Ilkotech Application Auth
$_CONFIG['site']['appSecret'] = "";

?>
