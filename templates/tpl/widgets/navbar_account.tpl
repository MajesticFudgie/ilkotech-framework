{Utils::addJS("//{{Core::$config['site']['static']}}/js/account_nav.js")}
<span class="header-account">
	{if core::loggedIn()}

	<span class="account-avatar">
		<a href="/"><img src="//avatar.ilkotech.co.uk/user/{{Core::$account['user']['username']}}"/></a>
	</span>

	<span class="account-info">
		<span class="account-name"><a href="/">{{Core::$account['user']['name']|htmlentities}}</a></span>
		<br/>
		<span class="account-rank"><a href="/logout">Logout</a> </span>
	</span>
	<div id="notifications">
		<div class="title">
			Notifications
		</div>
	</div>
	{else}
	<span class="account-avatar">
		<a href="/account"><img src="http://cravatar.eu/helmavatar/Steve/32"/></a>
	</span>
	<span class="account-info">
		<span class="account-name">Guest</span>
		<br/>
		<span class="account-rank"><a href="/login">Login</a> or <a href="/register">Register</a></span>
	</span>
	{/if}
</span>