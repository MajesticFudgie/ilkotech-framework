{if core::loggedIn()}
	{literal}
		<script type="text/javascript">CRISP_WEBSITE_ID = "af4d26b9-774b-4566-ac1f-b35d7ecef103";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.im/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
		<script>
			window.CRISP_READY_TRIGGER = function() {
				// Use $crisp safely there
	{/literal}
				$crisp.set("user:nickname","{{Core::$account['user']['name']}}");
				$crisp.set("user:email","{{Core::$account['user']['email']}}");
				
				
				$crisp.set("session:data",["Username","{{Core::$account['user']['username']}}"]);
				
				$crisp.set("session:data",["2FA Enabled?","No"]);
				
				{$code = Account::getField(Core::$account['user']['id'],"security-code")}
				$crisp.set("session:data",["Security Code","{if $code != null}{$code['value']}{else}None Set{/if}"]);
				
				{$code = Account::getField(Core::$account['user']['id'],"security-phrase")}
				$crisp.set("session:data",["Security Phrase","{if $code != null}{$code['value']}{else}None Set{/if}"]);
				
				{$apps = Account::authorizedApplications(Core::$account['user']['id'])}
				{$applist = array()}
				{foreach $apps as $id => $a}
					{$app = Applications::getApp($a['app'])}
					$crisp.set("session:data",["Auth'd App #{$id+1}","{{$app['name']}}"]);
				{/foreach}
	{literal}
			};
		</script>
	{/literal}
{/if}