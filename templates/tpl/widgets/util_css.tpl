{foreach Utils::getCSS() as $css}
	{if $css['url'] == null}
		<style>
			{{$css.content}}
		</style>
	{else}
		<link href="{{$css.url}}" rel="stylesheet"/>
	{/if}
{/foreach}