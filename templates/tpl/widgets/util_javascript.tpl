{foreach Utils::getJS() as $js}
	{if $js['url'] == null}
		<script>
			{{$js.content}}
		</script>
	{else}
		<script src="{$js.url}"></script>
	{/if}
{/foreach}