{extends file="base.tpl"}
{block name="content"}
<div id="content">
	<div class="content-title">
		<span class="title">Add Game</span>
	</div>
	<hr clas="sep">
	<form method="post" action="/add_game">
		<b>Name</b>
		<br/>
		<input class="input" size="30" placeholder="Full name of the Game" name="name">
		<p>
		<b>Barcode</b>
		<br/>
		<input class="input" size="30" placeholder="Scan or type the barcode (if any)" name="barcode" {if $barcode != null}value="{$barcode}"{/if}>
		<p>
		<b>Platform</b>
		<br/>
		<select class="input" name="platform">
			{foreach Games::getPlatforms() as $p}
				<option value="{$p.slug}">{$p.name|htmlentities}</option>
			{/foreach}
		</select>
		<p>
		<b>Owner</b>
		<br/>
		<select class="input" name="owner">
			{foreach Account::getUsers() as $u}
				<option {if $u.id == Core::$account.user.id}SELECTED{/if} value="{$u.userid}">{$u.name|htmlentities}</option>
			{/foreach}
		</select>
		<p>
		<b>Edition (<i>e.g. Collectors Edition</i>)</b>
		<br/>
		<input class="input" size="30" placeholder="Classic/Platinum/Definitive/Collectors Edition" name="desc"/>
		<p>
		<b>Notes (<i>e.g. No Box</i>)</b>
		<br/>
		<input class="input" size="30" placeholder="Anything to add?" name="notes"/>
		<p>
		<button type="submit" class="btn">Add Game</button>
	</form>
</div>
{/block}