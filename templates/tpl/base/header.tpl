<html>
	<head>
		<title>Ilkotech - Account Center</title>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-hQpvDQiCJaD2H465dQfA717v7lu5qHWtDbWNPvaTJ0ID5xnPUlVXnKzq7b8YUkbN" crossorigin="anonymous">
		<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
		<style>
			body {
				margin:0;
				padding:0;
				font-family:Raleway;
				background-color:whiteSmoke;
				color:#111;
			}
			#header {
				height:42px;
				width:100%;
				top:0;
				position:fixed;
				background-color:black;
				color:white;
				/* border-bottom:solid 3px red; */
			}
			#header .header-title {
				font-size:13px;
				margin-left:125px;
				line-height:42px;
			}
			#header .header-account {
				float:right;
				margin-right:125px;
				margin-top:5px;
			}
			#header .header-account .account-avatar img {
				border-radius:5%;
				height:32px;
			}
			#header .header-account .account-avatar .notification-count {
				background-color: red;
				height: 10px;
				min-width: 10px;
				border-radius: 15%;
				line-height: 10px;
				text-align: center;
				display: inline-block;
				font-size: 10px;
				color: white;
				position: relative;
				left: 34px;
				top: 22px;
				float: left;
				padding: 1px;
			}
			#header .header-account .account-info {
				float:right;
				margin-left:10px;
			}
			#header .header-account .account-info .account-rank {
				font-size:13px;
				color:silver;
			}
			
			#navigation {
				height:80px;
				background-color:#111;
				padding-top:42px;
				border-bottom:solid 3px red;
			}
			#navigation .website-brand {
				margin-left:125px;
				height:50px;
				width:50px;
				margin-top:15px;
				display:inline-block;
			}
			#navigation .website-brand img {
				height:50px;
			}
			#navigation .navbar {
				float:right;
				margin-right:125px;
				line-height:78px;
			}
			#navigation .navbar a {
				color:whiteSmoke;
				padding-left:20px;
				text-decoration:none;
				font-size:15px;
				display:inline-block;
			}
			#navigation .navbar a:hover {
				color:silver;
			}
			
			#sub-navigation {
				height:42px;
				background-color:#111;
			}
			#sub-navigation.empty {
				height:3px;
			}
			#sub-navigation .navbar {
				margin-left:125px;
				line-height:42px;
			}
			#sub-navigation .navbar a {
				color:whiteSmoke;
				padding-right:20px;
				text-decoration:none;
				font-size:15px;
				display:inline-block;
			}
			#sub-navigation .navbar a:hover {
				color:silver;
			}
			
			#page-banner {
				height:120px;
				background-image:url('//static.ilkotech.co.uk/img/hd_header_dark.png');
				background-size: cover;
				background-position:center;
			}
			#page-banner .banner-sub {
				margin-left:125px;
				display:inline-block;
				margin-top:37.5px;
				color:white;
			}
			#page-banner .banner-sub .title {
				font-size:27px;
			}
			#page-banner .banner-sub .subtitle {
				font-size:12px;
			}
			
			#page-banner .banner-main {
				margin-right:125px;
				display:inline-block;
				margin-top:37.5px;
				color:white;
				float:right;
				text-align:right;
			}
			#page-banner .banner-main .title {
				font-size:27px;
			}
			#page-banner .banner-main .subtitle {
				font-size:12px;
			}
			
			
			#content {
				margin-left:125px;
				margin-right:125px;
				margin-top:25px;
				padding-bottom:145px;
				overflow:auto;
			}
			
			#footer {
				background-color:#111;
				height:120px;
				width:100%;
				position:absolute;
				bottom:0px;
			}
			#footer .footer-sections {
				margin-left:125px;
			}
			#footer .footer-sections .section {
				width:200px;
				color:white;
				display:inline-table;
				font-size:15px;
				margin-top:15px;
				margin-right:30px;
			}
			
			#footer .footer-sections .section a {
				text-decoration:none;
				color:#6a89bf;
			}
			#footer .footer-sections .section hr {
				border-top:0.5px solid #8c8b8b;
				border-bottom:0px solid #fff;
			}
			
			#footer .footer-logo {
				float:right;
				margin-right:125px;
				height:55px;
				margin-top:32.5px;
			}
			#footer .footer-logo img {
				height:100%;
			}
			a {
				text-decoration:none;
				color:#6a89bf;
			}
			
			#page-wrapper {
				position:relative;
				min-height:100%;
			}
			
			
			#content .title {
				font-size:27px;
			}
			#content .content-title {
				line-height:27px;
			}
			.sep {
				border:solid 1px #111;
			}
			h1 > small {
				color:silver;
			}
		</style>
		<link href="//{Core::$config.site.static}/css/shared.css" rel="stylesheet"></link>
		{block name="css"}{/block}
		{include file="widgets/util_css.tpl"}
	</head>
	<body>
		<div id="page-wrapper">
			<div id="header">
				<span class="header-title">
					Ilkotech Account Center
				</span>
				{include file='widgets/navbar_account.tpl'}
			</div>
			<div id="navigation">
				<span class="website-brand"><a href="/"><img src="//{Core::$config.site.static}/img/singular_ilkotech.png"/></a></span>
				<span class="navbar">
					<a href="/my_games">My Games</a>
					<a href="/all_games">All Games</a>
					<a href="/add_game">Add Game</a>
					<a href="/platforms">Platforms</a>
				<span>
			</div>
			<div id="page-banner">
				<span class="banner-sub">
					<span class="title">{block name="title"}Home{/block}</span>
					<br>
					<span class="subtitle">{block name="subtitle"}All the games!{/block}</span>
				</span>
			</div>
			{block name="subnav"}
			<div id="sub-navigation" class="empty"></div>
			{/block}