<html>
	<head>
		<title>Ilkotech - Login</title>
		<!--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
		-->
		<link href="//{Core::$config.site.static}/css/shared.css" rel="stylesheet"></link>
		<style>
			body {
				background:#111;
				background-size:cover;
				background-position:center;
				font-family:"Raleway";
				/* background-image:url('//{Core::$config.site.static}/img/background_night.png'); */
				
			}
			.login {
				position:fixed;
				top:50%;
				left:50%;
				
				transform:translate(-50%,-50%);
				
				width:320px;
				color:white;
			}
			.login_logo {
				background-image:url('//{Core::$config.site.static}/img/logo_transparent.png');
				background-size:100% 100%;
				margin-bottom:20px;
				height:126px;
				width:320px;
			}
			.login_form {
				width: 100%;
				background:rgba(255,255,255,.25);
			}
			.login_form div.content {
				padding:10px;
			}
			.login_form .login_section {
				background:rgba(0,0,0,.25);
				padding:10px;
				font-size: 28px;
				min-height:28px;
				border-bottom:solid 1px red;
			}
		</style>
	</head>
	<body>
		<div class="login">
			<div class="login_logo"></div>
			<div class="login_form">
				<div class="login_section">
					<center>Sign In</center>
				</div>
				<form method="POST" action="/login">
					<div class="content">
					<input name="_token" type="hidden" value="{$token}"/>
					<div class="input-group">
						<h3>Username</h3>
						<input class="input full-width" name="username" id="username" placeholder="Username"/>
						{include file="base/format_error.tpl" error=Errors::getErr("username") blocking=true }
					</div>
					<div class="input-group">
						<h3>Password <a href="/recover" class="btn pull-right">Forgotten Password</a></h3>
						<input class="input full-width" name="password" type="password" placeholder="Password"/>
						{include file="base/format_error.tpl" error=Errors::getErr("password") blocking=true }
					</div>
					{include file="base/format_error.tpl" error=Errors::getErr("result") blocking=true }
					{if $token!=null}<input name="token" type="hidden" value="{$token}"/>{/if}
					</div>
					<div class="login_section">
						<a href="/register{if $token!=null}/{$token}{/if}" class="btn">Register</a> <button type="submit" class="btn btn-primary pull-right">Sign in</button>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha256-KXn5puMvxCw+dAYznun+drMdG1IFl3agK0p/pqT9KAo= sha512-2e8qq0ETcfWRI4HJBzQiA3UoyFk6tbNyG+qSaIBZLyW9Xf3sWZHN/lxe9fTh1U45DpPf07yj94KsUHHWe4Yk1A==" crossorigin="anonymous"></script>
</html>